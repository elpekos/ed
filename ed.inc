; **************************************************************************************************
; ed, command definitions and ORG address
;
; This file is part of the Z88 operating system, OZ.
;
; OZ is free software; you can redistribute it and/or modify it under the terms of the GNU General
; Public License as published by the Free Software Foundation; either version 2, or (at your option)
; any later version. OZ is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY
; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
; See the GNU General Public License for more details.
;
; (C) Thierry Peycru, 2022
;
; ***************************************************************************************************

defc    EXEC_ORG        = $2000                     ; Identify as relocatable code for ELF

defc    maxflnmsize     = 240

defvars 0
        stackptr        ds.w    1
        memhnd          ds.w    1
        filename        ds.w    1
        prevch          ds.b    1
        lnsize          ds.b    1
        parmblock                               ; Parameter block for 'gn_xin' call
        insert          ds.b    3
        current         ds.b    3
        next            ds.b    3
;        linebuffer0     ds.b    1               ; delimiter
;        linebuffer      ds.b    BUFLEN          ; Buffer for current line
        dummy           ds.b    6
        buf256          ds.b    256
        currow          ds.b    1               ; cursor current row
        SIZEOF_Workspace
enddef
