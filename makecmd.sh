#!/bin/sh

rm -f *.bin *.elf *.map *.err
mpm -bG -Rz80 -I../../oz/def ed.asm
if test $? -eq 0; then
    # program compiled successfully, apply leading Z80 ELF header
    mpm -bG -nMap -I../../oz/def -oed.elf ed-elf.asm
fi
