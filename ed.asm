; **************************************************************************************************
; Ed, the text editor
;
; This file is part of the Z88 operating system, OZ.
;
; OZ is free software; you can redistribute it and/or modify it under the terms of the GNU General
; Public License as published by the Free Software Foundation; either version 2, or (at your option)
; any later version. OZ is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY
; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
; See the GNU General Public License for more details.
;
; (C) Thierry Peycru, 2022
;
; ***************************************************************************************************

module  Ed

include "stdio.def"
include "fileio.def"
include "error.def"
include "memory.def"

include "ed.inc"

org EXEC_ORG

.entry
        ld      ix, 0
        add     ix, sp
        ld      (ws+stackptr), ix               ; preserve stackptr for exit

        ld      a, (ix+2)
        cp      2                               ; needs 1 argument
        jp      nz, ed_help                     ; argument is missing, display usage and exit

;       init parameter block for linked list

        ld      hl, ws+insert                   ; zero workspace
        ld      de, ws+insert+1
        ld      bc, 9+6+256-1                   ; and the buffer too
        ld      (hl), 0
        ldir

;       open memory pool

        push    ix
        ld      a, MM_S3 | MM_FIX | MM_MUL      ; chuncks from everywhere
        ld      bc, 0
        oz      OS_Mop
        ld      (ws+MemHnd), ix
        pop     ix
        jp      c, ed_error

;       try to open file for input

        ld      l, (ix+6)                       ; argv[1], the option
        ld      h, (ix+7)
        ld      (ws+filename), hl               ; store filename
        ld      de, ws+buf256
        ld      bc, $00FF
        ld      a, OP_IN
        oz      GN_Opf
        jr      c, $-1                          ; !! TODO : new file, ed_new
        call    ed_load
        oz      GN_Cl

;       cursor on

        oz      OS_Pout
        defm    1,"2+C",0

; -----------------------------------------------------------------------------
;
;       cursor top, first screen display
;
.ed_k_top
        call    ed_previous
        jr      c, $-1                          ; should not happen
        jr      z, eds_z                        ; first entry reached (empty one)
        jr      ed_k_top
.eds_z
        ld      a, FF                           ; cls
        oz      OS_Out
        ld      bc, $0800                       ; !! fix screen height for larger one
        jr      eds_0
.eds_1
        oz      OS_Nln
.eds_0
        push    bc
        call    ed_next                         ; on first iteration, goto first real line
        pop     bc
        jr      c, eds_2                        ; current = 0, it is the last one
        push    bc        
        call    ed_prline
        pop     bc
        inc     c
        djnz    eds_1
.eds_2
        ld      b, c
.eds_3
        push    bc
        call    ed_previous
        pop     bc
        djnz    eds_3
        oz      OS_Pout
        defm    1,"3@  ",0
        xor     a
        ld      (ws+currow), a

;       keyboard

.ed_osin
        oz      OS_In
        jr      nc, edi_0
        cp      RC_Susp
        jr      z, ed_osin
        jp      ed_error
.edi_0
        jr      z, ed_inzero

;       diamond commands

        cp      17                              ; <>Q
        jp      z, ed_quit
        jr      ed_osin

;       cursor dispatcher

.ed_inzero
        oz      OS_In
        cp      IN_DWN
        jr      z, ed_k_down
        cp      IN_UP
        jr      z, ed_k_up
        cp      IN_SDWN
        jp      z, ed_k_pagedown
        cp      IN_SUP
        jp      z, ed_k_pageup
        cp      IN_DDWN
        jr      z, ed_k_bottom
        cp      IN_DUP
        jr      z, ed_k_top
        jr      ed_osin

; -----------------------------------------------------------------------------
;
;       cursor up
;
.ed_k_up
        call    ed_previous
        jr      z, ed_kup1                      ; already at first line
        ld      a, (ws+currow)
        or      a
        jr      z, ed_scrollup
        dec     a
        ld      (ws+currow), a
        oz      OS_Pout
        defm    1,"2Y",0
        add     a, ' '
        oz      OS_Out
        jr      ed_osin
.ed_kup1
        call    ed_next
        jr      ed_osin

.ed_scrollup
        oz      OS_Pout
        defm    1,SD_UP
        defm    1,"3@",32,32,0
        call    ed_prline
        oz      OS_Pout
        defm    1,"3@",32,32,0
        jr      ed_osin

; -----------------------------------------------------------------------------
;
;       cursor down
;
.ed_k_down
        call    ed_next
        jr      c, ed_osin                      ; already at last line, stay here
        ld      a, (ws+currow)
        cp      7                               ; !! fix screen height for larger one
        jr      z, ed_scrolldown
        inc     a
        ld      (ws+currow), a
        oz      OS_Pout
        defm    1,"2Y",0
        add     a, ' '
        oz      OS_Out
        jp      ed_osin

.ed_scrolldown
        oz      OS_Pout
        defm    1,SD_DWN
        defm    1,"3@",32,32+7,0
        call    ed_prline
        oz      OS_Pout
        defm    1,"3@",32,32+7,0
        jp      ed_osin

; -----------------------------------------------------------------------------
;
;       cursor bottom
;
.ed_k_bottom
        call    ed_next                         ; goto next
        jr      nc, ed_k_bottom                 ; until last line
        ld      bc, $0701                       ; loops | counter
.edb_0
        push    bc
        call    ed_previous
        pop     bc
        jr      z, edb_1                        ; first line reached
        inc     c
        djnz    edb_0
.edb_1
        ld      a, FF
        oz      OS_Out
        ld      b, c
        jr      edb_3
.edb_2
        oz      OS_Nln
.edb_3
        push    bc
        call    ed_prline
        call    ed_next
        pop     bc
        jr      c, eds_4                        ; current = 0, it is the last one
        djnz    edb_2
.eds_4
        ld      a, c
        dec     a
        ld      (ws+currow), a
        oz      OS_Pout
        defm    1,"3@",32,0
        add     a, ' '
        oz      OS_Out
        jp      ed_osin

; -----------------------------------------------------------------------------
;
;       cursor page down
;
.ed_k_pagedown
        ld      a, (ws+currow)
        neg
        add     a, 7
        jr      z, edpd_1                       ; we're already at last line
        ld      b, a                            ; first we reach end of screen
.edpd_0
        push    bc
        call    ed_next
        pop     bc
        jr      c, ed_k_bottom
        djnz    edpd_0
.edpd_1
        call    ed_next
        jr      c, ed_k_bottom

        ld      a, FF                           ; cls
        oz      OS_Out
        ld      bc, $0800                       ; !! fix screen height for larger one
        jr      edpd_2
.edpd_3
        oz      OS_Nln
.edpd_2
        push    bc
        call    ed_prline
        call    ed_next
        pop     bc
        jr      c, edpd_4                       ; current = 0, it is the last one
        inc     c
        djnz    edpd_3
.edpd_4
        ld      a, (ws+currow)
        neg
        add     c
        jr      z, edpd_6
        jp      m, ed_k_bottom                  ; less lines displayed than cursor row
        ld      b, a
.edpd_5
        push    bc
        call    ed_previous
        pop     bc
        djnz    edpd_5
.edpd_6
        ld      a, (ws+currow)
        add     a, ' '
        oz      OS_Pout
        defm    1,"3@ ",0
        oz      OS_Out
        jp      ed_osin

; -----------------------------------------------------------------------------
;
;       cursor page up
;
.ed_k_pageup
        ld      a, (ws+currow)
        add     a, 8                            ; up of 8 lines
        ld      b, a                            ; first we reach top of screen
.edpu_0
        push    bc
        call    ed_previous
        pop     bc
        jr      z, edpu_1                       ; top of file, goto to top
        djnz    edpu_0

        ld      a, FF                           ; cls
        oz      OS_Out
        ld      b, 8                            ; !! fix screen height for larger one
        jr      edpu_7
.edpu_6
        push    bc
        call    ed_next
        pop     bc
        jp      c, ed_k_bottom                  ; this should not happen
        oz      OS_Nln
.edpu_7
        push    bc
        call    ed_prline
        pop     bc
        djnz    edpu_6
        ld      a, (ws+currow)
        neg
        add     a, 7
        ld      b, a
.edpu_9
        push    bc
        call    ed_previous
        pop     bc
        djnz    edpu_9
        oz      OS_Pout
        defm    1,"3@ ",0
        ld      a, (ws+currow)
        add     a, ' '
        oz      OS_Out
        jp      ed_osin

.edpu_1
        call    ed_next
        jr      c, $-1                          ; first line without next = empty file
        jp      ed_k_top

; -----------------------------------------------------------------------------
;
;       print a line, limited to 94 characters
;
.ed_prline
        ld      hl, (ws+current)
        ld      a, (ws+current+2)
        ld      b, a
        call    bhl_p3
        ld      c, 94
.edp_0
        oz      OS_Rbe
        or      a
        ret     z
        oz      OS_Out
        inc     hl
        dec     c
        jr      nz, edp_0
        ld      a, VT
        oz      OS_Out
        ret

; -----------------------------------------------------------------------------
;
;       load file in memory
;       handle : CR, CR+LF, LF
;
; IN:   IX = file handle
;
.ed_load
        oz      OS_Pout
        defm    FF,1,"3+FR Loading ",1,"4-FRC",0
        ld      a, LF                           ; initialize previous char

.ed_ldl
        ld      hl, ws+buf256
        ld      c, 4                            ; line counter (3 for list, 1 for terminator)
.ed_ldc
        ld      (ws+prevch), a
        oz      OS_Gb
        jr      nc, ed_ldnc
        cp      RC_Eof
        jr      z, ed_eof                       ; eof, line done, insert last one if not empty
        jp      ed_error                        ; another fatal error
.ed_ldnc
        cp      ' '
        jr      nc, ed_wch
        cp      CR
        jr      z, ed_wln
        cp      LF
        jr      nz, ed_ldc                      ; ignore other control chars
        ld      a, (ws+prevch)
        cp      CR
        ld      a, LF
        jr      z, ed_ldc                       ; ignore LF if CR+LF
        jr      ed_wln
.ed_wch
        ld      (hl), a
        inc     hl
        inc     c
        jr      nz, ed_ldc                      ; if line too long (>251), split it
        dec     c                               ; C = 255
.ed_wln
        push    af
        call    ed_iln
        pop     af
        jr      ed_ldl
.ed_eof
        ld      a, c
        cp      4
        ret     z                               ; line is empty, dont insert

;       insert line

.ed_iln
        ld      a, c
        sub     3                               ; remove 3 byte pointer
        ld      (ws+lnsize), a                  ; store line size
        ld      (hl), 0                         ; add null terminator
        push    ix                              ; preserve file handle
        ld      ix, (ws+MemHnd)
        ld      b, 0                            ; BC = total length
        xor     a
        oz      OS_Mal                          ; Allocate memory to hold current line
        pop     ix
        jp      c, ed_error                     ; If no memory available, finish

        ld      a, b
        ld      (ws+insert+2), a                ; (insert) = malloc
        ld      (ws+insert), hl                 ; Set up pointer to this block in 'insert'
        call    bhl_p3                          ; Add 3 to BHL to take us past link field

        ex      de, hl                          ; BDE = destination (after the link field)
        ld      hl, ws+buf256
        ld      a, (ws+lnsize)                  ; Retrieve size
        ld      c, a                            ; buffer content with null terminator
        oz      OS_Bde                          ; Copy line from buffer to allocated block

        ld      hl, ws+parmblock
        oz      GN_Xin                          ; Link in this block : insert/current/next
        ld      hl, (ws+insert)
        ld      (ws+current), hl                ; (current) = (insert)
        ld      a, (ws+insert+2)
        ld      (ws+current+2), a               ; Update current pointers
        ret

; -----------------------------------------------------------------------------
;
;       goto previous line
;
.ed_previous
        ld      de, (ws+next)
        ld      a, (ws+next+2)
        ld      c, a
        ld      hl, (ws+current)
        ld      a, (ws+current+2)
        ld      b, a
        oz      GN_Xnx                          ; Get next entry (in this case, the previous)
        ret     c
        ld      (ws+next), de
        ld      a, c
        ld      (ws+next+2), a
        ld      (ws+current), hl
        ld      a, b
        ld      (ws+current+2), a               ; Update pointers
        ret                                     ; return Fz

; -----------------------------------------------------------------------------
;
;       goto first line
;
.ed_next
        ld      de, (ws+current)
        ld      a, (ws+current+2)
        ld      c, a
        ld      hl, (ws+next)
        ld      a, (ws+next+2)
        ld      b, a
        oz      GN_Xnx                          ; Get next entry
        ret     c
        ld      (ws+current), de
        ld      a, c
        ld      (ws+current+2), a
        ld      (ws+next), hl
        ld      a, b
        ld      (ws+next+2), a                  ; Update pointers
        ret                                     ; return Fz

; -----------------------------------------------------------------------------
;
;       BHL = BHL+3
;
.bhl_p3                                         ; Add 3 to BHL
        ld      de, 3
        add     hl, de                          ; Add 3 to HL
        ret     nc
        inc     b                               ; In case of carry, increment B
        ret

; -----------------------------------------------------------------------------
;
;       Exit with error message
;
.ed_error
        oz      Gn_Esp                          ; report error to Shell window
        oz      OS_Nln
        oz      OS_Bout
        oz      OS_Nln
        scf
        jr      ed_exit

; -----------------------------------------------------------------------------
;
;       <>Q Quit command
;
.ed_quit                                        ; !! TODO : save if modified
        ld      a, FF
        oz      OS_Out

; -----------------------------------------------------------------------------
;
;       Exit, close memory pool and restore stack
;
.ed_exit
        ld      ix, (ws+MemHnd)
        oz      OS_Mcl                          ; close memory, will free allocated
        ld      hl, (ws+stackptr)
        ld      sp, hl                          ; restore stack
        ld      hl, 0                           ; Assume SH_OK
        ret     nc
        inc     hl                              ; return SH_ERR to Shell, for script-level post-processing
        cp      a                               ; return Fc = 0, this is not a system-level error
        ret

; -----------------------------------------------------------------------------
;
;       Command help message
;
.ed_help
        oz      OS_Pout
        defm    "Text editor, usage: ed ",1,"3+TUfilename",1,"3-TU",CR,LF,0
        jr      ed_exit                         ; Fc = 0

;------------------------------------------------------------------------------
; Workspace is located from here onwards (after end of program),
; allocated by Elf Loader via SIZEOF_Workspace
.ws
